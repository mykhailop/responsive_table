import React from 'react';
import styled from 'styled-components'

const StyledMenu = styled.div`
    width: 240px;
    min-width: 240px;
    position: sticky;
    left: 0;
    top: 0;
    background: #d2d2d2;
    height: 100vh;
`;

const StyledDiv = styled.div`padding: 1rem;`;

const StyledLi = styled.li`margin-bottom: 1rem`;

function Menu() {
    return (
        <StyledMenu>
            <StyledDiv>
                <span>This is menu. You can place here required information, navigation buttons, etc.</span>
                <ol>
                    <StyledLi>User has possibility to pin every column in table</StyledLi>
                    <StyledLi>You can set custom width for column in <code>utils/utils.ts</code>, just uncomment line 34</StyledLi>
                    <StyledLi>As for now user has possibility to sort data asc and desc. We could add possibility to turn off sort for current column or make multicolumn sort</StyledLi>
                </ol>
            </StyledDiv>
        </StyledMenu>
    );
}

export default Menu;
