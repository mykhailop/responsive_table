import React from 'react';
import styled from 'styled-components';

const styles = `
    filter: grayscale(1);
    opacity: 0.5;
`;

const StyledLoader = styled.div`
    position: absolute;
    top: 100px;
    left: 50%;
    border: 0.3rem solid #f3f3f3;
    border-radius: 50%;
    border-top: 0.3rem solid #3498db;
    width: 3rem;
    height: 3rem;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
    z-index: 100;
`;

interface LoaderProps {
    isLoading: boolean;
}

const WithLoader = styled.div`
    ${({ isLoading }: LoaderProps) => isLoading ? styles : ''}
`;

const Loader: React.FC<LoaderProps> = ({ isLoading, children }) => {
    return (
        <WithLoader isLoading={isLoading}>
            { isLoading ? <StyledLoader /> : null }
            { children }
        </WithLoader>
    );
};

export default Loader;
