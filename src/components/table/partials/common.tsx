export const pinnedStyles = `
    font-weight: bold;
    position: sticky;
    right: 0rem;
`;

export const commonCellStyles = `
    border: 1px solid #dcdada;
    padding: 0.5rem;
    display: flex;
    align-items: center;
`;

