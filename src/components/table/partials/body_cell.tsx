import React from 'react';
import styled from 'styled-components';
import { pinnedStyles, commonCellStyles } from './common';

interface BodyCellProps {
    column: string;
    text?: string;
    pinnedColumn?: string;
}

const StyledBodyCell = styled.div`
    background: white;
    ${commonCellStyles}
    ${({ pinnedColumn, column }: BodyCellProps) => pinnedColumn === column ? `${pinnedStyles}` : ''}
`;

function BodyCell({ text, column, pinnedColumn }: BodyCellProps) {
    return (
        <StyledBodyCell pinnedColumn={pinnedColumn} column={column}>{text || column}</StyledBodyCell>
    );
}

export default React.memo(BodyCell);
