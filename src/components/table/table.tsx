import React from 'react';
import { isObject, isFunction } from 'lodash';
import styled from 'styled-components';
import Header from './table_header';
import Body from './table_body';

import { TableConfig, SortDirection, ColumnsConfig } from '../../interfaces/interfaces.d';

interface TableProps {
    columns: ColumnsConfig[];
}

const Table = styled.div`
    display: grid;
    align-content: stretch;
    align-items: stretch;
    justify-items: stretch;
    min-height: 100px;
    width: 100%;
    grid-gap: 1px;
    border-bottom: 3px solid #c1c1c1;
    overflow-x: auto;
    ${({ columns }: TableProps) => `grid-template-columns: ${columns.map(entry => entry.width || 'auto').join(' ')}` }
`;

const getDirection = (currentDirection?: SortDirection): SortDirection => {
    return currentDirection === 'asc' ? 'desc' : 'asc';
};

function TableComponent({ body, columns, pinnedColumn, sortHandler, sort, pinHandler }: TableConfig) {
    const modifiedColumns = columns.filter(entry => entry.id !== pinnedColumn);
    const pinnedEntries = columns.filter(entry => entry.id === pinnedColumn);

    // pinned column should be always as last one
    const tableColumns = [ ...modifiedColumns, ...pinnedEntries ];

    return (
        <div>
            <Table columns={tableColumns}>
                <Header
                    columns={tableColumns}
                    sort={sort}
                    pinnedColumn={pinnedColumn}
                    clickHandler={(column: string) => {
                        if (!isFunction(sortHandler)) {
                            return;
                        }

                        const currentDirection = isObject(sort) ? sort[column] : undefined;

                        sortHandler(column, getDirection(currentDirection));
                    }}
                    pinHandler={pinHandler}
                />
                <Body
                    columns={tableColumns}
                    body={body}
                    pinnedColumn={pinnedColumn}
                />
            </Table>
        </div>
    );
}

export default TableComponent;
