import React from 'react';
import BodyCell from './partials/body_cell';

import { BodyConfig, ColumnsConfig } from '../../interfaces/interfaces.d';


function TableBody({ body, columns, pinnedColumn }: { body: BodyConfig[], columns: ColumnsConfig[], pinnedColumn?: string }) {
    return (
        <>
            { (body as BodyConfig[]).map((bodyEntry: BodyConfig, index: number) => {
                return (
                    <React.Fragment key={index}>
                        { (columns as ColumnsConfig[]).map((columnEntry: ColumnsConfig) => {
                            return (
                                <BodyCell
                                    key={columnEntry.id}
                                    column={columnEntry.id}
                                    text={bodyEntry[columnEntry.id]}
                                    pinnedColumn={pinnedColumn}
                                />
                            );
                        }) }
                    </React.Fragment>
                );
            }) }
        </>
    );
}

export default React.memo(TableBody);
