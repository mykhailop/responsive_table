import { keys, reduce, orderBy } from 'lodash';
import { TableConfig, SortDirection, BodyConfig } from '../interfaces/interfaces.d';

const makeTranslation = (token: string): string => token.split('_').join(' ');

const getSorted = ({ column, body, direction }: { column: string, body: BodyConfig[], direction: SortDirection }): BodyConfig[] => {
    // hack because of sort is made on UI part
    const intColumns = [ 'height', 'mass' ];

    return orderBy(body, (entry) => intColumns.includes(column) ? parseInt(entry[column], 10) : entry[column], [ direction ]);
};

export { getSorted };

const getTableConfig = async (): Promise<TableConfig> => {
    const peopleSchema = await fetch('https://swapi.dev/api/people/schema');
    const people = await fetch('https://swapi.dev/api/people');

    const schemaJson = await peopleSchema.json();
    const peopleJson = await people.json();

    const headerKeys = reduce(keys(schemaJson.properties), (memo: string[], key: string) => {
        const fieldsToOmit = [ 'created', 'edited' ];

        if (schemaJson.properties[key].type === 'string' && !fieldsToOmit.includes(key)) {
            return [ ...memo, key ];
        }

        return memo;
    }, []);

    const columns = headerKeys.map((key, index) => ({
        id: key,
        // width: index === 0 ? '450px' : undefined, // just mock custom width for one column
        translation: makeTranslation(key),
    }));

    return {
        columns,
        body: peopleJson.results,
        pinnedColumn: 'height',
    };
};

export { getTableConfig };
