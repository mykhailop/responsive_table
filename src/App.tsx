import React from 'react';
import { createGlobalStyle } from 'styled-components'
import Content from './modules/content/content';

const GlobalStyle = createGlobalStyle`
    body {
       margin: 0;
       background: whitesmoke;
       font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
        'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
        sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
`;

function App() {
    return (<>
        <GlobalStyle />
        <div>
            <Content />
        </div>
    </>);
}

export default App;
