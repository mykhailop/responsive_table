export type SortDirection = 'asc' | 'desc';

export interface ColumnsConfig {
    id: string;
    width?: string;
    translation?: string;
}

export interface BodyConfig {
    [key: string]: string;
}

export interface SortConfig {
    [column: string]: SortDirection;
}

export interface TableConfig {
    columns: ColumnsConfig[] | [];
    body: BodyConfig[] | [];
    pinnedColumn?: string;
    sort?: SortConfig;
    sortHandler?: (column: string, direction: SortDirection) => void;
    pinHandler?: (column: string) => void;
}

export interface HeaderCellProps {
    pinHandler?: (column: any) => void;
    clickHandler?: (column: any) => void;
    columnEntry: ColumnsConfig;
    sort?: SortConfig;
    pinnedColumn?: string;
}