import React, { useState, useEffect } from 'react';
import styled from 'styled-components'

import Menu from '../../components/menu/menu';
import Table from '../../components/table/table';
import Loader from '../../components/loader/loader';
import { getTableConfig, getSorted } from '../../utils/utils';
import { TableConfig, SortDirection } from '../../interfaces/interfaces.d';

// calc width 100% - menu width
const StyledContent = styled.div`
    font-size: 14px;
    width: calc(100% - 240px);
`;

const FlexDiv = styled.div`display: flex;`;
const PaddingDiv = styled.div`padding: 5rem;`;

function Content() {
    const defaultState: TableConfig = { body: [], columns: [] };

    const [ tableConfig, setTableConfig ] = useState(defaultState);
    const [ isLoading, setIsLoading ] = useState(false);
 
    useEffect(() => {
        const fetchData = async () => {
            const config = await getTableConfig();

            setTableConfig(config);
        };

        fetchData();        
    }, []);

    return (
        <FlexDiv>
            <Menu />
            <StyledContent>
                <PaddingDiv>
                    <Loader isLoading={isLoading}>
                        <Table
                            {...tableConfig}
                            sortHandler={(column: string, direction: SortDirection) => {
                                setIsLoading(true);

                                setTimeout(() => {
                                    setTableConfig({
                                        ...tableConfig,
                                        sort: { [column]: direction },
                                        body: getSorted({ column, body: tableConfig.body, direction }),
                                    });

                                    setIsLoading(false);
                                }, 1000);
                            }}
                            pinHandler={(column: string) => {
                                setTableConfig({
                                    ...tableConfig,
                                    pinnedColumn: column,
                                });
                            }}
                        />
                    </Loader>
                </PaddingDiv>
            </StyledContent>
        </FlexDiv>
    );
}

export default Content;
